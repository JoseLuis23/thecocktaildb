document.getElementById('btnBuscar').addEventListener('click', () => {
    const tipo = document.querySelector('input[name="coctel"]:checked').value;
    obtenerBebidas(tipo);
});

document.getElementById('btnLimpiar').addEventListener('click', () => {
    document.getElementById('resultado').innerHTML = '';
    document.getElementById('total').value = '';
});

async function obtenerBebidas(tipo) {
    try {
        const url = `https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=${tipo}`;
        const respuesta = await fetch(url); //fetch

        if (!respuesta.ok) {
            throw new Error(`Error al obtener los datos. Código: ${respuesta.status}`);
        }

        const datos = await respuesta.json();
        mostrarResultados(datos.drinks, tipo);
    } catch (error) {
        console.error(error.message);
    }
}
function mostrarResultados(cocteles, tipo) {
    const resultadoDiv = document.getElementById('resultado');
    resultadoDiv.innerHTML = '';

    if (cocteles) {
        cocteles.forEach(coctel => {
            const nombre = coctel.strDrink;
            const imagen = coctel.strDrinkThumb;

            const coctelDiv = document.createElement('div');
            coctelDiv.classList.add('drink'); 
            coctelDiv.innerHTML = `<img src="${imagen}" alt="${nombre}" width="100" height="100"><p>${nombre}</p>`;
            resultadoDiv.appendChild(coctelDiv);
        });

        document.getElementById('total').value = cocteles.length;
    } else {
        resultadoDiv.innerHTML = '<p>No se encontraron cocteles.</p>';
        document.getElementById('total').value = '0';
    }
}
